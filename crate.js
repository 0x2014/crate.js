
var http = require('http');
var options = {
    host: 'localhost',
    path: '_sql',
    port: '4200',
    method: 'POST'
};

var connect = function(host, port)
{
    options.host=host;
    options.port=port;
}

function query (sql, cbf)
{
    callback = function(response) {
        var str = ''
        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {
            var result = JSON.parse(str);
            if (result.error)
            {
                cbf (null,null,result.error);
                return;
            }
            var jsons =  result.rows.map (function (e) {
                var x = {};
                for (var i=0; i< result.cols.length;i++) {
                    x[result.cols[i]] = e[i];
                }
                return x;
            });
            cbf (jsons, result);
        });
    }

    var req = http.request(options, callback);
    // This is the data we are posting, it needs to be a string or a buffer
    req.write(JSON.stringify ({stmt:sql}));
    req.end();
}

exports.connect = connect;
exports.query = query;

