# crate.js

This is a simple client for [crate.io](https://crate.io) in node.js

It supports currently only the execution of SQL statements via the _sql API endpoint.
Results are returned in original format and as JSON objects. 

## How to use it: 

```
var crate = require ('./crate.js')
crate.query (process.argv[2] || "select * from tweets limit 1", function (jRows, rawResult, error) {
    if (!error)
    {
        console.log ('Rows as JSON objects:\n')
        console.log (jRows);
    } else  {
        console.log ("Error: \n" );
        console.log (error);
    }
});
```
 
